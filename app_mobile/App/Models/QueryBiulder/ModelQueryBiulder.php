<?php

namespace App\Models\QueryBiulder;

use App\Models\QueryBiulder\QueryBiulder;
use App\Database\Conexao;

class ModelQueryBiulder extends QueryBiulder{

	protected $conexao;

	public function __construct(){
		$this->conexao = Conexao::conectar();
	}



	public function all(){
        $pdo = $this->conexao->prepare($this->queryBiulder());
        if(!$pdo->execute()){
        	dump('Erro ao executar o sql '.$this->queryBiulder());
        }
        return $pdo->fetchAll();
	}


	public function first(){
        $pdo = $this->conexao->prepare($this->queryBiulder());
        if(!$pdo->execute()){
        	dump('Erro ao executar o sql '.$this->queryBiulder());
        }
        return $pdo->fetch();
	}

	public function count(){
        $pdo = $this->conexao->prepare($this->queryBiulder());
        if(!$pdo->execute()){
        	dump('Erro ao executar o sql '.$this->queryBiulder());
        }
        return $pdo->rowCount();
	}
}