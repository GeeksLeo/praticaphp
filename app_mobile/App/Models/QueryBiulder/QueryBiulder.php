<?php

namespace App\Models\QueryBiulder;

class QueryBiulder{

	private $select = null;
	private $from = null;
	private $where = null;
	private $join;
	private $order = null;

    
    //Metodos de utilização da classe
	public function select(){
		$selects = func_get_args();

		if (count($selects) > 0) {
			$this->select = implode(',', $selects);
		}

		return $this;
	}

	public function from($from){
		$this->from = $from;
		return $this;
	}

	public function where($field,$operation,$value){
		if(func_get_args() != 3){
			trigger_error('O metodo where precisa de 3 parâmetros');
			die();
		}

		$this->where = $field.' '.$operation.' '.$value;
		return $this;
	}

	public function join($table,$joins){
        if(func_get_args() != 2){
			trigger_error('O metodo join precisa de 2 parâmetros');
			die();
		}

		$this->join[$table] = $joins;
		return $this;
        
        //Estatico assim serve somente para 1 join
        //$this->join = ['table'=>$tabela,'join'=>$joins];
        //return $this;
	}

	public function order($field,$order){
        $this->order = $field.' '.$order;
	}






	//Metodo principal da classe
	protected function queryBiulder(){
		$query = 'select ';

		if(is_null($this->select)){
			$this->select = '*';
		}

		$query.= $this->select.' from ';

		if(is_null($this->from)){
			$this->from = $this->table;
		}

		$query.=$this->from;

		if (!is_null($this->join)) {
			foreach ($this->join as $table => $join) {
				$query.= ' inner join '.$table.' On '.$join;
			}
		}

		if (!is_null($this->where)) {
			$query.= ' where '.$this->where;
		}

		if (!is_null($this->order)) {
			$query.= ' order by '.$this->order;
		}

		return $query;
	}
}