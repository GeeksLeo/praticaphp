<?php

namespace App\Models;

use App\Models\QueryBiulder\ModelQueryBiulder;
use App\Traits\AttributesInsertUpdate;

class Model extends ModelQueryBiulder{
    
    use AttributesInsertUpdate;

	public function save(){
		//dump($this);
		$insertSQL = $this->insertSql();

		$insert = $this->conexao->prepare($insertSQL);

		foreach($this->data as $key=>$value){
			$insert->bindValue(":$key",$value);
		}

		$insert->execute();
		return $this->conexao->lastInsertId();
	}
}