<?php

namespace App\Traits;

trait AttributesInsertUpdate
{

	protected $data = [];
	//protected $attributes;


	public function __set($key,$value){
		$this->data[$key] = $value;
	}

	public function __get($key){
		return $this->data[$key];
	}

	private function insertKeys(){
		return implode(',',array_keys($this->data));
	}

	private function insertValues(){
        return ':'.implode(',:',array_keys($this->data));
	}

	public function insertSql(){
		return "INSERT INTO $this->table({$this->insertKeys()}) VALUES({$this->insertValues()})";
	}


}