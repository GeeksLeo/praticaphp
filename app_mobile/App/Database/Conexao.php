<?php

namespace App\Database;

use PDO;

class Conexao{

	public static function conectar(){
		$pdo = new PDO('mysql:host=localhost;dbname=finanseasy','root','');
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;
	}
}