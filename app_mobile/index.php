<?php
ini_set('display_errors',1);
require 'vendor/autoload.php';
?>
<?php
//Testes
//use App\Models\QueryBiulder\QueryBiulder;
//$t = new QueryBiulder;
//$t->from('contas');
//dump($t->queryBiulder());
?>
<?php
use App\Models\Back\Contas;
$contas = new Contas;

if(isset($_POST['salvar'])){
  $contas->cliente = $_POST['cliente'];
  $contas->op = $_POST['op'];
  $contas->valor = $_POST['valor'];
  $contas->save();

  header('Location: index.php');
}

$listados = $contas->select('cliente','op','valor')->all();

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	  <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="public/css/materialize.min.css"  media="screen,projection"/>
 
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/></head>
      <link rel="stylesheet" type="text/css" href="public/css/app.css">
      
      <!-- Mobile navBar !-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
      <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
      <link href="https://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.css" rel="stylesheet"/>


<body>
       <div data-role="page" id="home">
        <div data-role="header">
         <nav id="navbar">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">Financeasy</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="sass.html">Clientes</a></li>
                    <li><a href="badges.html">Lista de contas</a></li>
                    <li><a href="collapsible.html">Relatórios</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="sass.html">Clientes</a></li>
                    <li><a href="badges.html">Lista de contas</a></li>
                    <li><a href="collapsible.html">Relatórios</a></li>
                </ul>
            </div>
        </nav>

      

        </div>
          <a class="btn-floating btn-large waves-effect waves-light" href="#modal1" style="float:right;"><i class="material-icons">add</i></a>

          <!-- Modal Structure -->
          <div id="modal1" class="modal">
           <!--<div class="modal-content">-->
             <h4>Nova conta</h4>
             
             <div class="row">
               <form class="col s12" name="init" method="post">

               <div class="row">
                   <div class="input-field col s12">
                      <input name="cliente" type="text" id="autocomplete-input" class="autocomplete">
                      <label for="autocomplete-input">Selecione um cliente</label>
                   </div>
               </div>

                 <div class="row">
                   <div class="input-field col s12">
                     <input name="op" type="text" class="validate">
                     <label for="email" data-error="wrong" data-success="right">Operação</label>
                     
                   </div>
                 </div>
                   


                 <div class="row">
                   <div class="input-field col s5">
                     R$ <input type="number" name="valor" class="validate">
                   </div>
                 </div>      
                   
                    <input type="hidden" name="salvar">
	                <a href="!#" onclick="document.init.submit(); return false;" class=" modal-action modal-close waves-effect waves-light btn">Salvar</a>
	          </form>
             </div>


          <!--</div>-->
          <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
          </div>
          </div>
  
          <h4><?php echo $contas->count(); ?> contas</h4>

          <table class="bordered">
        <thead>
          <tr>
              <th data-field="id">Cliente</th>
              <th data-field="name">Operação</th>
              <th data-field="price">Valor</th>
          </tr>
        </thead>

        <tbody>
          <?php if($contas->count() <= 0): ?>
	        <td>Nenhum registro cadastrado</td>
	      <?php endif; ?>

	      <?php foreach($listados as $registro): ?>
	          <tr>
	            <td><?php echo $registro->cliente; ?></td>
	            <td><?php echo $registro->op; ?></td>
	            <td>R$ <?php echo number_format($registro->valor,2,',','.'); ?></td>
	          </tr>
	      <?php endforeach ?>
	     

        </tbody>
      </table>

       </div>
     


      <!--Import jQuery before materialize.js-->
        <script type="text/javascript">
        //Menu deslizando lateral INICIO
      	$( document ).ready(function(){
      	    $(".button-collapse").sideNav();
      	});
      	//Menu deslizando lateral FIM
        </script>

        <script type="text/javascript">
        //Modal botão principal INICIO
      	 $(document).ready(function(){
           // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
           $(".modal").modal();
         });
         </script>


         <script type="text/javascript">
        
         $(document).ready(function(){
           $("#modal1").modal('open');
         });
         </script>


         <script type="text/javascript">
         $(document).ready(function(){
           $("#modal1").modal('close');
         });
      	//Modal botão principal FIM
      </script>

      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="public/js/materialize.min.js"></script>
</body>
</html>