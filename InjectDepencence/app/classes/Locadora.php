<?php

namespace app\classes;

use \app\interfaces\Minterface;

class Locadora implements Minterface{

	public function listar(){
		return array(
           'Acao' => 'Filmes de acao',
           'Terror' => 'Filmes de terror',
           'Suspense' => 'Filmes de suspense'
	    );
	}
}