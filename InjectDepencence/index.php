<?php

ini_set('display_errors', 1);

define('ROOT',str_replace('\\','/', getcwd()));

require ROOT.'/vendor/autoload.php';

$biblioteca = new \app\classes\Biblioteca();
$midia = new \app\classes\midia($biblioteca);
$midia->encontrar();