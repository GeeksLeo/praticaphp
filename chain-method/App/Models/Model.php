<?php

namespace App\Models;

use App\Models\QueryBiulder\ModelQueryBiulder;
use App\Traits\AttributesInsertUpdate;

class Model extends ModelQueryBiulder{
	
	use AttributesInsertUpdate;

	public function save(){
        //dump($this);
		
		//Outro metodo
		//$insertSQL = $this->getAttributes($this)->insertSql();
		$insertSQL = $this->insertSql();

		$insert = $this->conexao->prepare($insertSQL);

		foreach($this->data as $key=>$value){
			$insert->bindValue(":$key",$value);
		}
		$insert->execute();
		return $this->conexao->lastInsertId();
	}

	public function update($id){
        $updateSQL = $this->updateSql();

        $update = $this->conexao->prepare($updateSQL);
        
        foreach($this->data as $key=>$value){
        	$update->bindValue(":$key",$value);
        }

        $update->bindValue(":id",$id);
        $update->execute();
        return $update->rowCount();
	}

}