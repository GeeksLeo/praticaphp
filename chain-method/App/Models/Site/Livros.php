<?php

namespace App\Models\Site;

use App\Models\Model;

class Livros extends Model{

	protected $table = 'tblivros';
 

    //Metodo reinscrito com alteração em regra de negocio
	public function update($id){
        $updateSQL = $this->updateSql();
        
        if ($id == 3) {
        	return 0;
        }
        $update = $this->conexao->prepare($updateSQL);
        
        foreach($this->data as $key=>$value){
        	$update->bindValue(":$key",$value);
        }

        $update->bindValue(":id",$id);
        $update->execute();
        return $update->rowCount();
	}
	
}