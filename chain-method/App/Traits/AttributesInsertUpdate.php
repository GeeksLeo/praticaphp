<?php
namespace App\Traits;

trait AttributesInsertUpdate
{
	protected $data = [];
	//protected $attributes;
    
    
	public function __set($key,$value){
		$this->data[$key] = $value;
	}

	public function __get($key){
		return $this->data[$key];
	}
   
   /* Outro metodo
    public function getAttributes($object){
        $this->attributes = call_user_func('get_object_vars',$object);
        return $this;
    }*/


	private function insertKeys(){
		return implode(',',array_keys($this->data));
	}

	private function insertValues(){
		return ':'.implode(',:',array_keys($this->data));
	}

	public function insertSql(){
		return "INSERT INTO $this->table({$this->insertKeys()}) VALUES({$this->insertValues()})";
	}

	public function updateSql(){
		$sql = "UPDATE {$this->table} SET ";
		
			foreach($this->data as $key=>$value){
				$sql.="$key=:$key,";
			}

		$sql = rtrim($sql,",");
		$sql.=' WHERE id=:id ';

		return $sql;
	}
}