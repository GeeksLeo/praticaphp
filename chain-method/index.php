<?php 
ini_set('display_errors',1);
require 'vendor/autoload.php';
use App\Models\Site\Livros;

/*Insert
$livrosModel = new Livros;
$livrosModel->titulo = 'Livro Teste com data';
$livrosModel->valor = '256';
$livrosModel->save();
*/

$livrosModel = new Livros;
$livrosModel->titulo = 'Livro atualizado 3';
$livrosModel->valor = '10500';
if($livrosModel->update(2) == 1){
	echo "Livro atualizado com sucesso !";
}else{
    echo "livro nao pode ser alterado";
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Chain Method</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

	<?php
	

	$livros = new Livros;
	$livrosCadastrados = $livros->select('id','titulo','valor')->all();
	//dump($livrosCadastrados);
	?>

    <div class="row">
    	<div class="col-md-12">
    		<h2>Temos até agora <?php echo $livros->count(); ?> livro cadastrado</h2>
    		<table class="table table-striped">
    			<thead>
    				<tr>
    					<th>Id</th>
    					<th>Livro</th>
    					<th>Valor</th>
    				</tr>
    			</thead>
    			<tbody>
	    			<?php if($livros->count() <= 0): ?>
	    			   <td>Nenhum livro cadastrado</td>
	                <?php endif; ?>

	                <?php foreach($livrosCadastrados as $livro): ?>
                        <tr>
                        	<td><?php echo $livro->id; ?></td>
                        	<td><?php echo $livro->titulo; ?></td>
                        	<td>R$ <?php echo number_format($livro->valor,2,',','.'); ?></td>
                        </tr>
	                <?php endforeach; ?>
    				
    			</tbody>
    		</table>
    	</div>
    </div>

</body>
</html>