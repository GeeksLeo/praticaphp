<?php

class Produto{
    private $descricao;
    private $estoque;
    private $preco;
    
    public function __construct($descricao,$estoque,$preco){
        $this->descricao = $descricao;
        $this->estoque = $estoque;
        $this->preco = $preco;
    }
    
    
    public function getDescricao(){
        return $this->descricao;
    }
    
    public function getEstoque(){
        return $this->estoque;
    }
    
    public function getPreco(){
        return $this->preco;
    }
    
}


$p1 = new Produto('Cerveja Belga',10,5);
echo $p1;