<?php
class Produto {
	private $descricao;
	private $estoque;
	private $preco;

	public function __construct($descricao,$estoque,$preco){
		$this->descricao = $descricao;
		$this->estoque = $estoque;
		$this->preco = $preco;

		print "CONSTRUIND: Objeto {$descricao}, Estoque {$estoque}, Preco {$preco}<br>";
	}

	public function __destruct(){
		print "DESTRUINDO: Objeto {$this->descricao}, Estoque {$this->estoque}, Preco {$this->preco}<br>";
	}
}


$p1 = new Produto('Chocolate',10,5);
unset($p1);
$p2 = new Produto('Cerveja',8,3);
unset($p2);
?>