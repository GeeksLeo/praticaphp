<?php

//namespace classes;

class Produto implements OrcavelInterface{
	 
	private $descricao;
	private $estoque;
	private $preco;
	private $fabricante;

	public function __construct($descricao,$estoque,$preco){
		$this->descricao = $descricao;
		$this->estoque = $estoque;
		$this->preco = $preco;
	}

	public function getDescricao(){
		return $this->descricao;
	}

    public function getPreco(){
    	return $this->preco;
    }
	public function setFabricante(Fabricante $f){
		$this->fabricante = $f;
	}

	public function getFabricante(){
		return $this->fabricante;
	}

	







	//Referente a aplicação de composição
	

	private $caracteristicas;

	public function addCaracteristica($nome,$valor){
		$this->caracteristicas[] = new Caracteristica($nome,$valor);
	}

	public function getCaracteristicas(){
		return $this->caracteristicas;
	}

	//Referente a aplicação de composição
}
 
?>