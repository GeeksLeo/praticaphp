<?php
require_once 'classes/Orcamento.php';
require_once 'classes/OrcavelInterface.php';
require_once 'classes/Produto.php';
require_once 'classes/Servico.php';


$o = new Orcamento;
$o->adiciona(new Produto('Maquina de café',10,299), 1);
$o->adiciona( new Produto('Barbeador eletrico',10,170), 1);
$o->adiciona( new Produto('Barra de chocolate',10,7), 3);

$o->adiciona(new Servico('Corte de grama',10), 2);
$o->adiciona( new Servico('Corte de barba',10), 1);
$o->adiciona( new Servico('Programação em Web',10), 3);

print $o->calculaTotal();