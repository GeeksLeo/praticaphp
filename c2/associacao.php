<?php

 use classes\Fabricante;
 use classes\Produto;

 //Criando os objetos
 $p1 = new Produto('Chocolate',10,7);
 $f1 = new Fabricante('Cacau Show','Willy Wonka','381812301');

 //Associação
 $p1->setFabricante($f1);

 print 'A descricao e '. $p1->getDescricao() .'<br>';
 print 'O Fabricante e '.$p1->getFabricante()->getNome().'<br>';

 ?>