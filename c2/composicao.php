<?php
 require_once 'classes/Produto.php';
 require_once 'classes/Caracteristica.php';

 //Criação dos objetos
 $p1 = new Produto('Chocolate',10,7);

 //Composição
 $p1->addCaracteristica('Cor','Branco');
 $p1->addCaracteristica('Peso','2.6 kg');
 $p1->addCaracteristica('Potencia','200 watts RMS');

 print 'Produto: '. $p1->getDescricao()."<br>\n";

 foreach ($p1->getCaracteristicas() as $C) {
 	print 'Caracteristica: '.$C->getNome().' - '. $C->getValor()."<br>\n";
 }
?>