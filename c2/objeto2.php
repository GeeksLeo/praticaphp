<?php
  class Produto{
  	public $descricao;
  	public $estoque;
  	public $preco;

  	public function aumentaEstoque($unidades){
  		if (is_numeric($unidades) AND $unidades >= 0) {
  			$this->estoque += $unidades;
  		}
  	}

  	public function diminuirEstoque($unidades){
  		if (is_numeric($unidades) AND $unidades >= 0) {
  			$this->estoque -= $unidades;
  		}
  	}

    
    public function reajustaPreco($percentual){
    	if (is_numeric($percentual) AND $percentual >= 0) {
    		$this->preco *= (1 + ($percentual/100));
    	}
    }
  }


  $p1 = new Produto;
  $p1->descricao = "Chocolate";
  $p1->estoque = 10;
  $p1->preco = 8;

  echo "O estoque de {$p1->descricao} e {$p1->estoque} e preco = R$ {$p1->preco} <br>";

  $p1->aumentaEstoque(10);
  $p1->diminuirEstoque(5);
  $p1->reajustaPreco(50);

  echo "Valores com ajustes <br>";
  echo "O estoque de {$p1->descricao} e {$p1->estoque} e preco = R$ {$p1->preco} <br>";
?>