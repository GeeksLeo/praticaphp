<?php 
  require_once 'vendor/autoload.php';  
  require_once 'vendor/php-activerecord/php-activerecord/ActiveRecord.php';

  $cfg = ActiveRecord\Config::instance();
  $cfg->set_model_directory('models');
  $cfg->set_connections(array('development' =>
   'mysql://root:@localhost/composer'));

  /*
  Create Metodo 1 - Instanciamneto de classe


  $user = new Users();
  $user->nome='Leonardo Teste 1';
  $user->save();
  */


  /*
  Create Metodo 2 - Utilizando o método estatico create
  
  $data = array(
     'nome' => 'Leo Freitas - Teste 2'
  );

  $user = Users::create($data);
  */

  
?>