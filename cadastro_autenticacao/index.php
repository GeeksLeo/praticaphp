<?php require 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Cadastro com autenticação</title>
	<link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/semantic.css">
	<link rel="stylesheet" type="text/css" href="bower_components/font-awesome/css/font-awesome.css">
</head>
<body>
    
    <div class="ui container">
        <h2><i class="fa fa-user"></i> Lista de usuários cadastrados e autenticados</h2>

        <table class="ui select table">
            <thead>
        	   <tr>
        		<th>Nome</th>
        		<th>Email</th>
               </tr>
         	</thead>
         	<tbody>
         	   <tr>
        		 <th></th>
        		 <th></th>
        	   </tr>
         	</tbody>
        </table>

        <div class="ui divider"></div>

        <h2><i class="fa fa-pencil-squer-o"></i>Cadastro com autenticação</h2>

        <form action="" method="post" class="ui form">
        	<div class="field">
        		<label>Dados do usuário</label>
        		<div class="two fields">
        			<div class="field">
        				<input type="text" name="nome" placeholder="Nome">
        			</div>
        			<div class="field">
        				<input type="email" name="email" placeholder="Email">
        			</div>
        		</div>
        		<input type="submit" class="ui orange button" value="Cadastrar usuário">
        	</div>
        </form>
    </div>

    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
</body>
</html>