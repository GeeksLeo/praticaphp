<?php

namespace Acme\Classes;

class Email{

	private $email;
	private $quem;
	private $para;
	private $assunto;
	private $mensagem;

	public function setQuem($quem){
		$this->quem = $quem;
	}

	public function setPara($para){
		$this->para = $para;
	}

	public function setAssunto($assunto){
		$this->assunto = $assunto;
	}

	public function setMensagem($mensagem){
		$this->mensagem = $mensagem;
	}

	public function __construct(){
		$this->email = new \PHPMailer;
	}

	public function enviarEmail(){
		$this->email->Charset = 'UTF-8';
		$this->email->SMTPSecure = 'ssl';
		$this->email->isSMTP();
		$this->email->host = '';
		$this->email->Port = '';
		$this->email->SMTPAuth = true;
		$this->email->Username = '';
		$this->email->Password = '';
	}
}